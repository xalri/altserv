extern crate libalt;
extern crate reffers;
extern crate futures;
extern crate tokio_core;
extern crate tokio_periodic;
#[macro_use] extern crate slog;

use std::time::*;
use libalt::proto::ping::*;
use libalt::proto::rules::MessageRule;
use libalt::net::socket::*;
use libalt::proto::Context;
use tokio_core::reactor::*;
use futures::Stream;
use tokio_periodic::PeriodicTimer;

pub fn main() {
   let log = logger::example("ping");

   let mut reactor = Core::new().unwrap();

   enum Ev{ Timer, Msg(Message) }

   let socket = Socket::bind(
      ([0,0,0,0], 27290).into(),
      reactor.handle(),
      MessageRule(Context::Client),
      log.clone()).unwrap();

   let mut server = Server::new(log, Sender(socket.sender()));
   server.start_game("ffa_cave.altx").unwrap();

   let msg = socket
      .map(|msg| Ev::Msg(Message(msg)))
      .map_err(|e| panic!("oops: {}", e));

   let timer = PeriodicTimer::new(&reactor.handle()).unwrap();
   timer.reset(Duration::from_millis(33)).unwrap();
   let timer = timer
      .map(|_| Ev::Timer)
      .map_err(|e| panic!("Timer error: {}", e));

   let ev = msg.select(timer).for_each(move |ev| {
      match ev {
         Ev::Timer => server.update(),
         Ev::Msg(message) => server.process(&message),
      }
      Ok(())
   });

   reactor.run(ev).unwrap();
}

use reffers::rc::*;

use libalt::util::{Log, logger};
use libalt::model::types::VERSION;
use libalt::model::server_conf::FFAConfig;
use libalt::proto::message::{ServerInfo, Message, Data, Info, Join};
use libalt::proto::ping::*;
use libalt::proto::Sender;
use libalt::resources::shared_map::*;
use libalt::resources::dir_source::*;
use libalt::server::error::*;
use libalt::server::client::Client;
use libalt::server::game::*;
use libalt::server::game::game_modes::*;
use libalt::server::processors::*;

pub struct Server {
   pub pings: Pings,
   pub file_server: FileServer,
   pub server_list: ListJoiner,
   pub limbo: Limbo<Game>,
   pub map_source: MapSource,
   pub games: Vec<Strong<Game>>,
   pub log: ::Log,
   sender: Sender,
}

impl Server {
   /// Create a new server
   pub fn new(log: ::Log, sender: Sender) -> Server  {
      Server {
         map_source: MapSource::new(DirSource::open("/opt/altitude/maps/").unwrap(), 32),
         file_server: FileServer::new(log.clone(), sender.clone()),
         server_list: ListJoiner::new(log.clone(), sender.clone()),
         pings: Pings::new(sender.clone()),
         limbo: Limbo::new(sender.clone()),
         games: Vec::new(),
         sender,
         log,
      }
   }

   pub fn start_game(&mut self, map_name: &str) -> Result<()> {
      let map = self.map_source.get_map(map_name.to_string())?;

      self.file_server.add_to_cache(
         map.path().to_owned(),
         map.altx().compressed().to_vec());

      let game = Game::new(
         self.log.clone(),
         self.sender.clone(),
         map,
         FFA::new(self.log.clone(), FFAConfig::default()));

      self.games.push(Strong::new(game));

      Ok(())
   }

   pub fn info(&self) -> ServerInfo {
      ServerInfo {
         game_id: 0,
         addr: None,
         map_name: None,
         max_players: 0,
         num_players: 0,
         server_name: Some("libalt test".to_owned()),
         pass_req: false,
         hardcore: true,
         min_level: 1,
         max_level: 60,
         disallow_demo: false,
         version: Some(VERSION),
      }
   }

   pub fn update(&mut self) {
      for game in &self.games { game.get_mut().tick(); }
      self.pings.update();
      self.server_list.update();

      for lost in self.limbo.lost() {
         match lost {
            Lost::Disconnected(_client) => (),
            Lost::Timeout(_client) => (),
            Lost::Lost(_client) => unreachable!(),
         }
      }
   }

   /// Handle an incoming message.
   pub fn process(&mut self, msg: &Message) {
      self.pings.process(msg);
      self.file_server.process(msg);
      self.server_list.process(msg);
      for game in &self.games { game.get_mut().process(msg); }

      let addr = msg.addr();
      match *msg.data() {
         Data::Join(Join::JoinReq(ref req)) => {
            let req = req.clone();
            if req.version != VERSION {
               let data = Join::JoinResp(Some("Client and server versions don't match.".to_string()));
               self.sender.send(msg.reply(Data::Join(data)));
               return;
            }

            let data = Join::JoinResp(None);
            self.sender.send(msg.reply(Data::Join(data)));

            let log = self.log.new(o!("client" => format!("{}", addr)));
            let client = Client::<()>::new(log, addr, req.id, req.level,
                                           self.games[0].get().config().clone());

            self.limbo.add_client(client, self.games[0].get_weak());
         }
         Data::ServerInfo(Info::Request) => {
            let response = Data::ServerInfo(Info::Response(self.info()));
            self.sender.send(msg.reply(response));
         }
         _ => (),
      }
   }
}

